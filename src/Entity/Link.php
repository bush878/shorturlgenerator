<?php

namespace App\Entity;

use App\Repository\LinkRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * Сущность - ссылка.
 *
 * @ORM\Entity(repositoryClass=LinkRepository::class)
 */
class Link
{
    /**
     * Оригинальная ссылка.
     *
     * @ORM\Column(type="string", length=255)
     *
     * @var string|null
     */
    private $original_link = null;

    /**
     * Хеш ссылки.
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="string", length=8)
     *
     * @var string|null
     */
    private $hash = null;

    /**
     * Возвращает оригинальную ссылку.
     *
     * @return string|null
     */
    public function getOriginalLink(): ?string
    {
        return $this->original_link;
    }

    /**
     * Устанавливает оригинальную ссылку.
     *
     * @param string $originalLink Оригинальная ссылка.
     *
     * @return $this
     */
    public function setOriginalLink(string $originalLink): self
    {
        $this->original_link = $originalLink;

        return $this;
    }

    /**
     * Возвращает хеш ссылки.
     *
     * @return string|null
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * Устанавливает хеш ссылки.
     *
     * @param string $hash Хеш ссылки.
     *
     * @return $this
     */
    public function setHash(string $hash): self
    {
        $this->hash = $hash;

        return $this;
    }
}
