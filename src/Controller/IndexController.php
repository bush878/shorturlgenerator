<?php

namespace App\Controller;

use App\Entity\Link;
use App\Form\LinkForm;
use App\Service\LinkService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Дефолтный контроллер приложения.
 */
class IndexController extends AbstractController
{
    private const INDEX_ACTION = 'index';

    /**
     * Сервис управления ссылками.
     *
     * @var LinkService
     */
    private $linkService;

    public function __construct(LinkService $linkService)
    {
        $this->linkService = $linkService;
    }

    /**
     * Экшн редиректа.
     *
     * @Route("/{hash?}", name="home")
     *
     * @param Request $request HTTP запрос.
     *
     * @return Response
     */
    public function redirectByUrl(Request $request): Response
    {
        $hash = $request->get('hash');
        if (static::INDEX_ACTION === $hash) {
            return $this->index($request);
        }

        $link = $this->linkService->getByHash($hash);
        if ($link instanceof Link) {
            return $this->redirect($link->getOriginalLink());
        }

        return (new Response('', Response::HTTP_NOT_FOUND));
    }

    /**
     * Дефолтный экшн.
     *
     * @Route("/index", name="index")
     *
     * @param Request $request HTTP запрос.
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        $link = new Link();
        $form = $this->createForm(LinkForm::class, $link);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            $existingLink = $this->linkService->getByOriginalLink($link->getOriginalLink());
            if ($existingLink instanceof Link){
                return $this->render(
                    'index/index.html.twig',
                    [
                        'form'     => $form->createView(),
                        'shortUrl' => $request->getHttpHost() . '/' .$existingLink->getHash(),
                    ]
                );
            }

            $link = $this->linkService->save($link);
        }

        $hash = null;
        if ($link instanceof Link) {
            $hash = $link->getHash();
        }

        return $this->render(
            'index/index.html.twig',
            [
                'form'     => $form->createView(),
                'shortUrl' => $hash ? $request->getHttpHost() . '/' .$link->getHash() : '',
            ]
        );
    }
}
