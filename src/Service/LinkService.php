<?php

namespace App\Service;

use App\Entity\Link;
use App\Repository\LinkRepository;
use Doctrine\ORM\EntityManagerInterface;
use Throwable;

/**
 * Сервис управления ссылками.
 */
final class LinkService
{
    /**
     * @var LinkRepository
     */
    private $linkRepository;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(LinkRepository $linkRepository, EntityManagerInterface $entityManager)
    {
        $this->linkRepository = $linkRepository;
        $this->entityManager = $entityManager;
    }

    /**
     * Сохраняет объект ссылки в базу.
     *
     * @param Link $link Объект ссылки.
     *
     * @return Link
     */
    public function save(Link $link): Link
    {
        try {
            $this->entityManager->persist($link);
            $this->entityManager->flush();
        } catch (Throwable $exception) {
            return null;
        }

        return $this->getByOriginalLink($link->getOriginalLink());
    }

    /**
     * Возвращает объект ссылки по ее хешу.
     *
     * @param string $hash Хеш.
     *
     * @return Link|null
     */
    public function getByHash(string $hash): ?Link
    {
        return $this->linkRepository->findOneBy(['hash' => $hash]);
    }

    /**
     * Возвращает объект ссылки по ее оригинальному значению.
     *
     * @param string $originalLink Оригинальная ссылка.
     *
     * @return Link|null
     */
    public function getByOriginalLink(string $originalLink): ?Link
    {
        return $this->linkRepository->findOneBy(['original_link' => $originalLink]);
    }
}