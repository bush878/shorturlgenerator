## Запрос на создание таблицы:
```
CREATE TABLE link (
    hash VARCHAR(8) NOT NULL DEFAULT LEFT(UUID(), 8),
    original_link VARCHAR(255) NOT NULL,
    PRIMARY KEY(hash))
DEFAULT CHARACTER SET utf8mb4
COLLATE `utf8mb4_unicode_ci`
ENGINE = InnoDB;
```

## Запрос на создание индекса:
```
CREATE UNIQUE INDEX link_original_link_idx ON link (original_link);
```

Пример работы в example.jpg.




